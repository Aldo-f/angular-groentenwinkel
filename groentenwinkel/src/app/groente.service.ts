import { Observable, of } from 'rxjs';

import { GROENTEN } from './mock-groenten';
import { Injectable } from '@angular/core';
import { Groente } from './groente';

@Injectable({
  providedIn: 'root'
})
export class GroenteService {

  getGroenten(): Observable<Groente[]> {
    return of(this._convertGroenten(GROENTEN));
  }

  private _convertGroenten(lijst: any[]) {
    const groentenLijst: Groente[] = [];
    lijst.forEach((groente: any[]) => {
      const g = {
        naam: groente[0],
        prijs: groente[1],
        eenheid: groente[2]
      } as Groente;
      groentenLijst.push(g);
    });
    return groentenLijst;
  }
}
