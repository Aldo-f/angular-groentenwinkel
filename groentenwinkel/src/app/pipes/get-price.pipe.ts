import { Pipe, PipeTransform } from '@angular/core';

import { PriceUnit } from '../bestelling';

@Pipe({ name: 'getPrice' })
export class GetPricePipe implements PipeTransform {
    transform(prijs: number, priceUnit: PriceUnit, aantal?: number): number {
        if (aantal) {
            if (priceUnit === PriceUnit.EUR) {
                return aantal * prijs;
            } else {
                return prijs * this.getCurrentExchangeRate() * aantal;
            }
        } else {
            // subtotaal
            if (priceUnit === PriceUnit.EUR) {
                return prijs;
            } else {
                return prijs * this.getCurrentExchangeRate();
            }
        }

    }

    getCurrentExchangeRate(): number {
        // TODO: get current exchangeRate (with timeout)
        // https://free.currconv.com/api/v7/convert?q=EUR_USD&compact=ultra&apiKey=c02a2dfc50901392dab4

        // let data: any;
        // fetch('https://free.currconv.com/api/v7/convert?q=EUR_USD&compact=ultra&apiKey=c02a2dfc50901392dab4')
        //     .then(response => {
        //         return response.json();
        //     })
        //     .then(myJson => {
        //         data = myJson;
        //         console.log(data);
        //         return data.EUR_USD;
        //     });

        return 1.107616;
    }
}
