import { TestBed } from '@angular/core/testing';

import { GroenteService } from './groente.service';

describe('GroenteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GroenteService = TestBed.get(GroenteService);
    expect(service).toBeTruthy();
  });
});
