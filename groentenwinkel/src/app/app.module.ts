import { } from '@angular/forms'; // <-- NgModel lives here

import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AccordionComponent } from './bootstrap/accordion/accordion.component';
import { AlertComponent } from './bootstrap/alert/alert.component';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BestelOverviewComponent } from './bestel-overview/bestel-overview.component';
import { BestellingOverviewComponent } from './bestelling-overview/bestelling-overview.component';
import { BootstrapComponent } from './bootstrap/bootstrap.component';
import { BrowserModule } from '@angular/platform-browser';
import { GetPricePipe } from './pipes/get-price.pipe';
import { GroenteOverviewComponent } from './groente-overview/groente-overview.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WinkelOverviewComponent } from './winkel-overview/winkel-overview.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BootstrapComponent,
    AccordionComponent,
    AlertComponent,
    WinkelOverviewComponent,
    BestelOverviewComponent,
    GroenteOverviewComponent,
    BestellingOverviewComponent,
    GetPricePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
