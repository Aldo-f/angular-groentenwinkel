import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { BestelOverviewComponent } from './bestel-overview.component';

describe('BestelOverviewComponent', () => {
  let component: BestelOverviewComponent;
  let fixture: ComponentFixture<BestelOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BestelOverviewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BestelOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
