import { BestelLijn, Bestelling } from '../bestelling';
import { Component, ElementRef, OnInit } from '@angular/core';

import { Winkel } from '../winkel';
import { Groente } from '../groente';

interface Alert {
  type: string;
  message: string;
}
@Component({
  selector: 'app-bestel-overview',
  templateUrl: './bestel-overview.component.html',
  styleUrls: ['./bestel-overview.component.scss']
})
export class BestelOverviewComponent implements OnInit {

  selectedGroente: Groente;
  selectedWinkel: Winkel;
  selectedAantal: number;
  totaal: number;

  isInputValid = false;
  bestelling: Bestelling = {};

  alerts: Alert[];

  constructor() {
    this.bestelling.bestelLijnen = [];
  }

  ngOnInit() {

  }

  onSubmit() {

    // disabled can be removed in front
    if (this.isValid()) {
      console.log('Alles is valid');

      // maak bestelLijn
      const bestelLijn: BestelLijn = { winkel: this.selectedWinkel, groente: this.selectedGroente, aantal: this.selectedAantal }
      const upTeDatenBestelLijn = this.bestelling.bestelLijnen
        .find((bestelLijn: BestelLijn) => bestelLijn.groente === this.selectedGroente && bestelLijn.winkel === this.selectedWinkel);

      if (upTeDatenBestelLijn) {
        upTeDatenBestelLijn.aantal += this.selectedAantal;
      } else {
        this.bestelling.bestelLijnen.push(bestelLijn);
      }

      // Bereken totaal
      this.bestelling.totaal = 0;
      this.bestelling.bestelLijnen
        .forEach((bestelLijn: BestelLijn) =>
          this.bestelling.totaal += bestelLijn.aantal * bestelLijn.groente.prijs);
      // for (const bestellijn of this.bestelling.bestelLijnen) {
      //   this.bestelling.totaal += bestellijn.aantal * bestellijn.groente[1];
      // }

    } else {
      console.log('niet alles is valid');
    }


  }

  // Validatie
  isValid() {
    const winkelValid = this.isDefined(this.selectedWinkel);
    const groenteValid = this.isDefined(this.selectedGroente);
    if (groenteValid) {
      const aantalValid = this.isValidNumber(this.selectedAantal, this.selectedGroente.eenheid);
      // return true if all is true, else false
      return winkelValid && groenteValid && aantalValid;
    }
    return false;
  }

  checkSubmitButton() {
    this.isInputValid = this.isValid();
    // if (this.isValid()) {
    //   // remove disabled on button
    //   this.isInputValid = true;
    // } else {
    //   this.isInputValid = false;
    // }
  }

  isDefined(value: any) {
    return value !== undefined;
  }

  isValidNumber(value: number, groenteUnit: string) {
    if (!isNaN(value) && value > 0) {
      if (groenteUnit !== 'kg') {
        // round number
        return value % 1 === 0;
      } else {
        // decimal number
        return true;
      }
    } else {
      // geen value (zoals undefined of 'e') of kleiner dan 0
      return false;
    }
  }


  // Haal waardes op
  setSelectedAantal(aantal: number) {
    this.selectedAantal = aantal;
    this.checkSubmitButton();

    // TODO: maak beter;
    if (this.isDefined(this.selectedGroente)) {
      this.checkNumberError();
    } else {
      this.alerts = null;
    }
  }

  setSelectedGroente(groente: Groente) {
    this.selectedGroente = groente;
    this.checkSubmitButton();
    if (!this.isDefined(this.selectedGroente)) {
      console.log('Groente not valid');
    } else {
      if (this.selectedAantal) {
        this.checkNumberError();
      }
    }
  }

  setSelectedWinkel(winkel: Winkel) {
    this.selectedWinkel = winkel;
    this.checkSubmitButton();
    if (!this.isDefined(this.selectedWinkel)) { console.log('Winkel not valid'); }
  }

  checkNumberError() {
    if (!this.isValidNumber(this.selectedAantal, this.selectedGroente.eenheid)) {
      console.log('Number not valid');
      this.alerts = [{
        type: 'danger',
        message: 'Ongeldig aantal gekozen',
      }];
    } else {
      this.alerts = null;
    }
  }
}
