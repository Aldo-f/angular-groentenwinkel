import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BestellingOverviewComponent } from './bestelling-overview.component';

describe('BestellingOverviewComponent', () => {
  let component: BestellingOverviewComponent;
  let fixture: ComponentFixture<BestellingOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BestellingOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BestellingOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
