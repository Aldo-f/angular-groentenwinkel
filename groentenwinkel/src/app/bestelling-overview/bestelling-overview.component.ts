import { Bestelling, PriceUnit } from '../bestelling';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-bestelling-overview',
  templateUrl: './bestelling-overview.component.html',
  styleUrls: ['./bestelling-overview.component.scss']
})
export class BestellingOverviewComponent implements OnInit {

  @Input() bestelling: Bestelling = {};

  priceUnitEnum = PriceUnit;
  priceUnit: PriceUnit = PriceUnit.EUR;

  constructor() { }

  ngOnInit() {
  }

  changeUnit(unit: PriceUnit) {
    this.priceUnit = unit;
  }


}
