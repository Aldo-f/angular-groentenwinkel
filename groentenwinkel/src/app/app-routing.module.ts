import { RouterModule, Routes } from '@angular/router';

import { BestelOverviewComponent } from './bestel-overview/bestel-overview.component';
import { BootstrapComponent } from './bootstrap/bootstrap.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  { path: '', component: BestelOverviewComponent },

  { path: 'bootstrap', component: BootstrapComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
