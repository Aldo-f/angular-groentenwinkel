import { Observable, of } from 'rxjs';

import { Injectable } from '@angular/core';
import { WINKELS } from './mock-winkels';
import { Winkel } from './winkel';

@Injectable({
  providedIn: 'root'
})
export class WinkelService {

  getWinkels(): Observable<Winkel[]> {
    return of(WINKELS);
  }

}
