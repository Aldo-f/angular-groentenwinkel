import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { GroenteService } from '../groente.service';
import { Groente } from '../groente';

@Component({
  selector: 'app-groente-overview',
  templateUrl: './groente-overview.component.html',
  styleUrls: ['./groente-overview.component.scss']
})
export class GroenteOverviewComponent implements OnInit {
  groenten: Groente[];
  selectedGroente: Groente;
  selectedAantal: number;

  constructor(private groenteService: GroenteService) { }

  @Output()
  changeAantal: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  changeGroente: EventEmitter<Groente> = new EventEmitter<Groente>();

  ngOnInit() {
    this.getGroenten();
  }

  getGroenten(): void {
    this.groenteService.getGroenten()
      .subscribe(groenten => this.groenten = groenten);
  }

  clickSelect(event: any): void {
    this.selectedGroente = this.groenten.find((g: Groente) => g.naam === event.target.value);
    this.changeGroente.emit(this.selectedGroente);
  }

  changeInput(event: any): void {   
    // geen controle if key is 'tab'
    if (event.key == 'Tab') { return }
    const selectedAantal = +event.target.value;
    this.changeAantal.emit(selectedAantal);
  }

}
