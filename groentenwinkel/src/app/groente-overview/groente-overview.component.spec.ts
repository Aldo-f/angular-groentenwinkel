import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { GroenteOverviewComponent } from './groente-overview.component';

describe('GroenteOverviewComponent', () => {
  let component: GroenteOverviewComponent;
  let fixture: ComponentFixture<GroenteOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GroenteOverviewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroenteOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
