import { TestBed } from '@angular/core/testing';

import { WinkelService } from './winkel.service';

describe('WinkelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WinkelService = TestBed.get(WinkelService);
    expect(service).toBeTruthy();
  });
});
