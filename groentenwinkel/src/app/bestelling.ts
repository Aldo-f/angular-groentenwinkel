import { Winkel } from './winkel';
import { Groente } from './groente';

export class BestelLijn {
    winkel: Winkel;
    groente: Groente;
    aantal: number;
}

export class Bestelling {
    bestelLijnen?: BestelLijn[];
    totaal?: number;
}

export enum PriceUnit {
    EUR = 'EUR',
    USD = 'USD'
}
