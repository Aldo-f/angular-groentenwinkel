import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { WinkelOverviewComponent } from './winkel-overview.component';

describe('WinkelOverviewComponent', () => {
  let component: WinkelOverviewComponent;
  let fixture: ComponentFixture<WinkelOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WinkelOverviewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WinkelOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
