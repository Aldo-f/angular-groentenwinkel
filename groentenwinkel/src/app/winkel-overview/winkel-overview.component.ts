import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { WINKELS } from 'src/app/mock-winkels';
import { Winkel } from 'src/app/winkel';
import { WinkelService } from '../winkel.service';

@Component({
  selector: 'app-winkel-overview',
  templateUrl: './winkel-overview.component.html',
  styleUrls: ['./winkel-overview.component.scss']
})
export class WinkelOverviewComponent implements OnInit {
  winkels: Winkel[];

  selectedWinkel: Winkel;
  constructor(private winkelService: WinkelService) { }

  @Output() changeWinkel: EventEmitter<any> = new EventEmitter<any>();

  getWinkels(): void {
    this.winkelService.getWinkels()
      .subscribe(winkels => this.winkels = winkels);
  }

  ngOnInit() {
    this.getWinkels();
  }

  clickSelect(event: any): void {
    const selectedWinkel = this.winkels.find((w: Winkel) => w.naam === event.target.value);
    this.changeWinkel.emit(selectedWinkel);
  }
}
