# Groentenwinkel
Maak een groentenwinkel bestaande uit verschillende groenten van verschillende winkels.<br>

## Functioneel
* formulier
  * uit winkel, # groenten (uit de txt's te halen)
    * naam winkel (+ adres bij mousover)
    * naam groente + prijs/eenheid
    * bestelknop
    * bij aanvullen van gelijke groenten wordt het aantal in de bestellijn aangepast (gelijke groenten gecombineerd)
  * validatie 
    * winkel && groente gekozen
    * geldig aantal (>0 && decimaal enkel mogelijk bij groenten die ``prijs/kg`` verkocht worden)
    * indien ongeldig &rarr; bestelknop op disabled
* mogelijkheid tot wissel van valuta (de wissel wordt onthouden, startvaluta is euro)


## Visueel 
* (ng-)bootstrap 

## Werkwijze


<hr>

```
// terminal
npm install -g @angular/cli

ng help
ng generate --help
ng build groentenwinkel -c production
npm start
npm install bootstrap
```
```
// angular.json
"styles": [
  "node_modules/bootstrap/dist/css/bootstrap.min.css"
]
```
```
// terminal
npm install -g --save-dev @angular-devkit/build-angular
npm install angular-in-memory-web-api --save
npm install @angular/http --save
npm install --save @ng-bootstrap/ng-bootstrap
npm install --save rxjs-compat
```

* [angular tutorial ](https://angular.io/tutorial)
* [ng-bootstrap](https://ng-bootstrap.github.io/#/getting-started)